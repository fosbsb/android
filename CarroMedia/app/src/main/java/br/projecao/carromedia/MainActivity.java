package br.projecao.carromedia;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText etCarro;
    private EditText etLitros;
    private EditText etValor;
    private EditText etDistancia;
    private Button btCalcular;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etCarro  = (EditText) findViewById(R.id.etCarro);
        etLitros = (EditText) findViewById(R.id.etLitros);
        etValor  = (EditText) findViewById(R.id.etValor);
        etDistancia = (EditText) findViewById(R.id.etDistancia);
        btCalcular  = (Button) findViewById(R.id.btCalcular);

        btCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Float litros = Float.parseFloat(etLitros.getText().toString());
                Float valor  = Float.parseFloat(etValor.getText().toString());
                Float distancia = Float.parseFloat(etDistancia.getText().toString());

                Float custoTanque = litros*valor;
                Float media       = distancia/litros;
                Float custoKm     = custoTanque/distancia;

                Intent resultado = new Intent(getBaseContext(),ResultadoActivity.class);
                resultado.putExtra("carro",etCarro.getText().toString());
                resultado.putExtra("litros",litros);
                resultado.putExtra("valor",valor);
                resultado.putExtra("distancia",distancia);
                resultado.putExtra("custo_tanque",custoTanque);
                resultado.putExtra("media",media);
                resultado.putExtra("custo_km",custoKm);

                startActivity(resultado);



            }
        });

















    }
}
