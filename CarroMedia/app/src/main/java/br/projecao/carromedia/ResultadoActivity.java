package br.projecao.carromedia;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ResultadoActivity extends AppCompatActivity{

    private TextView tvCarro;
    private TextView tvLitros;
    private TextView tvValor;
    private TextView tvDistancia;
    private TextView tvCustoTanque;
    private TextView tvMedia;
    private TextView tvCustoKm;
    private Button btFinalizar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);

        tvCarro = (TextView) findViewById(R.id.tvCarro);
        tvLitros = (TextView) findViewById(R.id.tvLitros);
        tvValor = (TextView) findViewById(R.id.tvValor);
        tvDistancia = (TextView) findViewById(R.id.tvDistancia);
        tvCustoTanque = (TextView) findViewById(R.id.tvCustoTanque);
        tvMedia = (TextView) findViewById(R.id.tvMedia);
        tvCustoKm = (TextView) findViewById(R.id.tvCustoKm);
        btFinalizar = (Button) findViewById(R.id.btFinalizar);

        tvCarro.setText(getIntent().getStringExtra("carro"));
        tvLitros.setText(getIntent().getFloatExtra("litros",2)+" L");
        tvValor.setText("R$ "+getIntent().getFloatExtra("valor",2));
        tvDistancia.setText(getIntent().getFloatExtra("distancia",2)+" KM");
        tvCustoTanque.setText("R$ "+getIntent().getFloatExtra("custo_tanque",2));
        tvMedia.setText(getIntent().getFloatExtra("media",2)+" KM");
        tvCustoKm.setText("R$ "+getIntent().getFloatExtra("custo_km",0));

        btFinalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }
}
