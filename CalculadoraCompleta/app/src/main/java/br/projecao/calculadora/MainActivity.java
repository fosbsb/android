package br.projecao.calculadora;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    //Declarando Variáveis de forma Global para Classe.

    private EditText etNumero1;
    private EditText etNumero2;
    private Button btSomar;
    private Button btSubtrair;
    private Button btMultiplicar;
    private Button btDividir;
    private Integer numero1;
    private Integer numero2;
    private TextView tvResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Recuperação dos recursos de Tela via metodo findViewById.
        etNumero1       = (EditText) findViewById(R.id.etNumero1);
        etNumero2       = (EditText) findViewById(R.id.etNumero2);
        tvResultado     = (TextView) findViewById(R.id.tvResultado);
        btSomar         = (Button) findViewById(R.id.btSomar);
        btSubtrair      = (Button) findViewById(R.id.btSubtrair);
        btDividir       = (Button) findViewById(R.id.btDividir);
        btMultiplicar   = (Button) findViewById(R.id.btMultiplicar);

        btSomar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pegarDados();
                mostrarResultado("Resultado: " + (numero1+numero2));
            }
        });

        btSubtrair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pegarDados();
                mostrarResultado("Resultado: " + (numero1-numero2));
            }
        });

        btDividir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pegarDados();

                try {
                    mostrarResultado("Resultado: " + (numero1/numero2));
                }catch (Exception e){
                    Toast.makeText(MainActivity.this, "Favor digitar um número maior que 0.",
                            Toast.LENGTH_LONG).show();
                }

            }
        });

        btMultiplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pegarDados();
                mostrarResultado("Resultado: " + (numero1*numero2));
            }
        });

    }

    public void pegarDados(){
        //buscando informação dos EditText de Entrada de dados
        numero1 = Integer.parseInt(etNumero1.getText().toString());
        numero2 = Integer.parseInt(etNumero2.getText().toString());
    }

    public void limparTela(){
        //Limpando os campos e retornando o cursor para o primeiro EditText
        etNumero1.setText("");
        etNumero2.setText("");
        etNumero1.requestFocus();
    }

    public void mostrarResultado(String resultado){
        tvResultado.setText(resultado);
        limparTela();
    }
}
