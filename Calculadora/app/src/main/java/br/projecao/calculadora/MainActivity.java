package br.projecao.calculadora;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    //Declarando Variáveis de forma Global para Classe.

    private EditText etNumero1;
    private EditText etNumero2;
    private Button btCalcular;
    private TextView tvResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Recuperação dos recursos de Tela via metodo findViewById.
        etNumero1 = (EditText) findViewById(R.id.etNumero1);
        etNumero2 = (EditText) findViewById(R.id.etNumero2);
        btCalcular = (Button) findViewById(R.id.btCalcular);
        tvResultado = (TextView) findViewById(R.id.tvResultado);

        // Ativar o Ouvinte OnClickListener para habilitar o Click do Botão btCalcular.
        btCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //buscando informação dos EditText de Entrada de dados
                Integer numero1 = Integer.parseInt(etNumero1.getText().toString());
                Integer numero2 = Integer.parseInt(etNumero2.getText().toString());

                Integer soma = numero1 + numero2;

                //Setando o resultado no TextView
                tvResultado.setText(numero1 + " + " + numero2 + " = " + soma);

                //Limpando os campos e retornando o cursor para o primeiro EditText
                etNumero1.setText("");
                etNumero2.setText("");
                etNumero1.requestFocus();
            }
        });

    }
}
