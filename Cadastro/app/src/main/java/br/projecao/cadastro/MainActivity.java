package br.projecao.cadastro;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {

    private EditText etNome;
    private EditText etTelefone;
    private RadioGroup rgSexo;
    private Spinner spCurso;
    private Button btCadastrar;
    private RadioButton rbSelecionado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etNome = (EditText) findViewById(R.id.etNome);
        etTelefone = (EditText) findViewById(R.id.etTelefone);
        rgSexo = (RadioGroup) findViewById(R.id.rgSexo);
        spCurso = (Spinner) findViewById(R.id.spCurso);
        btCadastrar = (Button) findViewById(R.id.btCadastrar);


        btCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String nome = etNome.getText().toString();
                String telefone = etTelefone.getText().toString();
                rbSelecionado = (RadioButton) findViewById(rgSexo.getCheckedRadioButtonId());
                String sexo = rbSelecionado.getText().toString();
                String curso = spCurso.getSelectedItem().toString();

                Intent cadastro = new Intent(getBaseContext(),
                        CadastroActivity.class);
                cadastro.putExtra("nome", nome);
                cadastro.putExtra("telefone",telefone);
                cadastro.putExtra("sexo",sexo);
                cadastro.putExtra("curso",curso);

                startActivity(cadastro);


            }
        });


    }
}
