package br.projecao.cadastro;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CadastroActivity extends AppCompatActivity{

    private TextView tvNome;
    private TextView tvTelefone;
    private TextView tvSexo;
    private TextView tvCurso;
    private Button btFechar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        tvNome     = (TextView) findViewById(R.id.tvNome);
        tvTelefone = (TextView) findViewById(R.id.tvTelefone);
        tvCurso    = (TextView) findViewById(R.id.tvCurso);
        tvSexo     = (TextView) findViewById(R.id.tvSexo);
        btFechar   = (Button) findViewById(R.id.btFechar);

        String nome     = getIntent().getStringExtra("nome");
        String telefone = getIntent().getStringExtra("telefone");
        String sexo     = getIntent().getStringExtra("sexo");
        String curso    = getIntent().getStringExtra("curso");

        tvNome.setText(nome);
        tvTelefone.setText(telefone);
        tvSexo.setText(sexo);
        tvCurso.setText(curso);

        btFechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }
}
